
## 功能介绍
[wangmarket（网市场云建站系统）](https://gitee.com/mail_osc/wangmarket) 的 生成整站插件，自动生成 sitemap.xml 文件
当网站点击生成整站后，会自动生成当前网站的sitemap.xml文件。生成网站后，你可以访问你的 域名/sitemap.xml  即可看到


## 使用条件
1. 本项目(生成的 target/wangmarket-plugin-xxx.jar)放到 [网市场云建站系统](https://gitee.com/leimingyun/wangmarket_deploy) 中才可运行使用
1. 网市场云建站系统本身需要 v5.7 或以上版本。
1. 本项目只支持 mysql 数据库。使用默认 sqlite 数据库的不可用（需要标注一下支持哪种数据库。如果是这个插件涉及到数据表、以及数据表字段方面的改动，那就只支持mysql数据库。如果没有数据表及字段属性的改动，那就支持 mysql、sqlite数据库）

## 使用方式
1. 下载 /target/ 中的jar包
1. 将下载的jar包放到 tomcat/webapps/ROOT/WEB-INF/lib/ 下
1. 重新启动运行项目，登陆网站管理后台，点击生成整站， 然后访问 域名/sitemap.xml  即可看到(注意，此不会出现在网站管理后台的功能插件下)


## 二次开发
#### 本插件的二次开发
1. 运行项目，随便登陆个网站管理后台（默认运行起来后，会自带一个默认账号密码都是 wangzhan 的账号，可以用此账号登陆）
1. 登陆网站管理后台后，找到左侧的 生成整站 ，按照步骤操作即可。

#### 从头开始开发一个自己的插件
参考文档：  https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3213258&doc_id=1101390

#### 二次开发wangmarket及功能的扩展定制
可参考：  https://gitee.com/leimingyun/wangmarket_deploy

## 合作洽谈
作者：管雷鸣<br/>
微信：xnx3com<br/>
QQ：921153866<br/>
